
emojipedia\_search
==================================================
[![PyPI version](https://badge.fury.io/py/emojipedia_search.svg)](https://badge.fury.io/py/emojipedia_search)
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

A command-line utility that searches for unicode characters using https://emojipedia.org website.

